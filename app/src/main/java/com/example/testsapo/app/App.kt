package com.example.testsapo.app

import android.app.Activity
import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import java.util.HashSet

class App : MultiDexApplication() {
    private var allActivities: MutableSet<Activity>? = null


    override fun onCreate() {
        super.onCreate()
        instance = this
    }


    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    fun addActivity(act: Activity) {
        if (allActivities == null) {
            allActivities = HashSet()
        }
        allActivities!!.add(act)
    }

    fun removeActivity(act: Activity) {
        if (allActivities != null) {
            allActivities!!.remove(act)
        }
    }

    fun exitApp() {
        if (allActivities != null) {
            synchronized(allActivities!!) {
                for (act in allActivities!!) {
                    act.finish()
                }
            }
        }
        android.os.Process.killProcess(android.os.Process.myPid())
        System.exit(0)
    }

    companion object {
        @get:Synchronized
        var instance: App? = null
            private set
    }
}
