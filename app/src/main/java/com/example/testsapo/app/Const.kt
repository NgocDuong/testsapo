package com.example.testsapo.app

import android.os.Environment
import com.example.testsapo.model.bean.Profile
import java.io.File

object Const {
    //================= PATH ====================

    val PATH_DATA = App.instance!!.cacheDir.absolutePath + File.separator + "data"

    val PATH_CACHE = PATH_DATA + "/NetCache"

    val PATH_SDCARD =
        Environment.getExternalStorageDirectory().absolutePath + File.separator + "codeest" + File.separator + "GeekNews"
    val DB_NAME = "app.db"

    val profile : Profile = Profile("","","","","","","")

    interface Preference {
        companion object {
            const val KEY = "APP"
        }
    }

}