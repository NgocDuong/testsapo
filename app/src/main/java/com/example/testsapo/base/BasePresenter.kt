package com.example.testsapo.base

interface BasePresenter< out T> {

    fun attachView(view: Any?)

    fun detachView()
}
