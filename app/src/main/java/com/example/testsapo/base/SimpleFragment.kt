package com.example.testsapo.base

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import butterknife.ButterKnife
import butterknife.Unbinder
import com.afollestad.materialdialogs.MaterialDialog
import com.example.testsapo.R
import com.kaopiz.kprogresshud.KProgressHUD
import me.yokeyword.fragmentation.SupportFragment
import permissions.dispatcher.PermissionRequest

abstract class SimpleFragment : SupportFragment() {

    protected lateinit var mView: View
    protected lateinit var mActivity: Activity
    protected lateinit var mContext: Context
    private var mUnBinder: Unbinder? = null
    protected var isInited = false
    private var mKProgressHUD: KProgressHUD? = null
    protected abstract val layoutId: Int


    @SuppressLint("MissingSuperCall")
    override fun onAttach(context: Context) {
        mActivity = context as Activity
        mContext = context
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(layoutId, null)
        return mView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mUnBinder = ButterKnife.bind(this, view)
        mKProgressHUD = KProgressHUD.create(mContext)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setCancellable(false)
            .setAnimationSpeed(2)
            .setDimAmount(0.5f)
    }
    protected fun showRationaleDialog(@StringRes messageResId: Int, request: PermissionRequest) {
        MaterialDialog(mContext)
            .show {
                message(messageResId)
                positiveButton(R.string.yes, click = {
                    request.proceed()
                })
                negativeButton(R.string.no, click = {
                    request.cancel()
                })
            }
    }
    override fun onLazyInitView(savedInstanceState: Bundle?) {
        super.onLazyInitView(savedInstanceState)
        isInited = true
        initEventAndData()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mUnBinder!!.unbind()
    }

     fun showProgressDialog() {
        mKProgressHUD!!.show()
    }

     fun dismissProgressDialog() {
        if (mKProgressHUD != null && mKProgressHUD!!.isShowing) {
            mKProgressHUD!!.dismiss()
        }
    }

    protected abstract fun initEventAndData()
}