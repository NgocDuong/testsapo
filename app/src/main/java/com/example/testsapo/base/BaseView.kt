package com.example.testsapo.base


interface BaseView {

    fun showErrorMsg(msg: String)

    fun useNightMode(isNight: Boolean)

    //=======  State  =======
    fun stateError()

    fun stateEmpty()

    fun stateLoading()

    fun stateMain()
    fun showProgressDialog()
    fun dismissProgressDialog()
}
