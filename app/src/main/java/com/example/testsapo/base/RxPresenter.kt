package com.example.testsapo.base

import com.example.testsapo.utils.RxBus
import com.example.testsapo.utils.Service
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer

open class RxPresenter<T : BaseView> : BasePresenter<T> {

    protected var mView: T? = null
    protected var mCompositeDisposable: CompositeDisposable? = null
    private var rxBusKeys = ArrayList<String>()
    protected fun unSubscribe() {
        if (mCompositeDisposable != null) {
            mCompositeDisposable!!.clear()
        }
        for (key: String in rxBusKeys) {
            RxBus.get(key)?.unSubscribe()
        }
    }

    protected fun addSubscribe(subscription: Disposable) {
        if (mCompositeDisposable == null) {
            mCompositeDisposable = CompositeDisposable()
        }
        mCompositeDisposable!!.add(subscription)
    }

    protected fun addSubscribeBus(key: String): Service? {
        rxBusKeys.add(key)
        return RxBus.get(key)
    }

    @Suppress("UNCHECKED_CAST")
    override fun attachView(view: Any?) {
        this.mView = view as T?

    }

    override fun detachView() {
        this.mView = null
        unSubscribe()
    }
}
