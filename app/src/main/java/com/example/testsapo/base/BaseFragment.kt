package com.example.testsapo.base

import android.os.Bundle
import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter


abstract class BaseFragment<T : BasePresenter<*>> : SimpleFragment(), BaseView {

    protected var mPresenter: T? = null

    protected abstract val presenter: T

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mPresenter = presenter
        mPresenter!!.attachView(this)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        if (mPresenter != null) mPresenter!!.detachView()
        super.onDestroyView()
    }

    override fun showErrorMsg(msg: String) {
        //        SnackbarUtil.show(((ViewGroup) getActivity().findViewById(android.R.id.content)).getChildAt(0), msg);
    }

    override fun useNightMode(isNight: Boolean) {

    }

    override fun stateError() {

    }

    override fun stateEmpty() {

    }

    override fun stateLoading() {}

    override fun stateMain() {

    }

    protected fun setMore(baseQuickAdapter: BaseQuickAdapter<*, *>, size: Int) {
        if (size < 10) {
            baseQuickAdapter.loadMoreEnd()
            baseQuickAdapter.setEnableLoadMore(false)
            baseQuickAdapter.removeAllFooterView()
        } else {
            baseQuickAdapter.loadMoreComplete()
        }
    }
}