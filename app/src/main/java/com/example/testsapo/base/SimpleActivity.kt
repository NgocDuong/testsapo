package com.example.testsapo.base

import android.app.Activity
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.appcompat.widget.Toolbar
import butterknife.ButterKnife
import butterknife.Unbinder
import com.afollestad.materialdialogs.MaterialDialog
import com.example.testsapo.R
import com.example.testsapo.app.App
import com.kaopiz.kprogresshud.KProgressHUD
import me.yokeyword.fragmentation.SupportActivity
import permissions.dispatcher.PermissionRequest

abstract class SimpleActivity : SupportActivity() {

    protected lateinit var mContext: Activity
    private var mUnBinder: Unbinder? = null
    private var mKProgressHUD: KProgressHUD? = null

    protected abstract val layout: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout)
        mUnBinder = ButterKnife.bind(this)
        mContext = this
        onViewCreated()
        App.instance!!.addActivity(this)
        initEventAndData()
    }

    protected fun setToolBar(toolbar: Toolbar, title: String) {
        toolbar.title = title.toUpperCase()
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationOnClickListener { onBackPressedSupport() }
    }

    protected open fun onViewCreated() {
        mKProgressHUD = KProgressHUD.create(mContext)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setCancellable(false)
            .setAnimationSpeed(2)
            .setDimAmount(0.5f)
    }

     fun showProgressDialog() {
        mKProgressHUD!!.show()
    }

     fun dismissProgressDialog() {
        if (mKProgressHUD != null && mKProgressHUD!!.isShowing) {
            mKProgressHUD!!.dismiss()
        }
    }
    protected fun showRationaleDialog(@StringRes messageResId: Int, request: PermissionRequest) {
        MaterialDialog(mContext)
            .show {
                message(messageResId)
                positiveButton(R.string.yes, click = {
                    request.proceed()
                })
                negativeButton(R.string.no, click = {
                    request.cancel()
                })
            }
    }

    override fun onDestroy() {
        super.onDestroy()
        App.instance!!.removeActivity(this)
        mUnBinder!!.unbind()
    }

    protected abstract fun initEventAndData()
}
