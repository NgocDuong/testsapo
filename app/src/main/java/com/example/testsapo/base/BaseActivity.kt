package com.example.testsapo.base

import androidx.appcompat.app.AppCompatDelegate
import com.chad.library.adapter.base.BaseQuickAdapter


abstract class BaseActivity<T : BasePresenter<*>> : SimpleActivity(), BaseView {

    protected var mPresenter: T? = null



    override fun onViewCreated() {
        super.onViewCreated()
        mPresenter = getPresenter()
        if (mPresenter != null)
            mPresenter!!.attachView(this)
    }

    override fun onDestroy() {
        if (mPresenter != null)
            mPresenter!!.detachView()
        super.onDestroy()
    }

    override fun showErrorMsg(msg: String) {
        //        SnackbarUtil.show(((ViewGroup) findViewById(android.R.id.content)).getChildAt(0), msg);
    }

    override fun useNightMode(isNight: Boolean) {
        if (isNight) {
            AppCompatDelegate.setDefaultNightMode(
                AppCompatDelegate.MODE_NIGHT_YES
            )
        } else {
            AppCompatDelegate.setDefaultNightMode(
                AppCompatDelegate.MODE_NIGHT_NO
            )
        }
        recreate()
    }

    override fun stateError() {

    }

    override fun stateEmpty() {

    }

    override fun stateLoading() {

    }

    override fun stateMain() {

    }

    protected fun setMore(baseQuickAdapter: BaseQuickAdapter<*, *>, size: Int) {
        if (size < 10) {
            baseQuickAdapter.loadMoreEnd()
            baseQuickAdapter.setEnableLoadMore(false)
            baseQuickAdapter.removeAllFooterView()
        } else {
            baseQuickAdapter.loadMoreComplete()
        }
    }

    abstract fun getPresenter(): T?
}