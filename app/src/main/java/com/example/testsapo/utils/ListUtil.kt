package com.example.testsapo.utils

import java.util.*
import java.util.function.Predicate

data class ListItem<T>(var t: T?, var index: Int)

inline fun <reified T> List<*>.asListOfType(): List<T>? =
    if (all { it is T })
        @Suppress("UNCHECKED_CAST")
        this as List<T> else
        null

fun <T> List<T?>?.compare(other: List<T?>?): Boolean {
    if (this == other) {
        return true
    } else if (this != null && other != null && this.size == other.size) {
        for (i in this.indices) {
            if (!this[i].compare(other[i])) return false
        }
        return true
    }
    return false
}

fun <T> T?.compare(other: T?): Boolean {
    return this?.equals(other) ?: false
}

fun <T, E> List<T>.select(condition: (T) -> E?): List<E> {
    val result = ArrayList<E>()
    if (isNotEmpty()) {
        for (item in this) {
            val itemE = condition(item)
            if (itemE != null) {
                result.add(itemE)
            }
        }
    }
    return result
}

fun <T> List<T>.forEach(condition: (T, Int) -> Unit) {
    if (isNotEmpty()) {
        for ((i, item) in this.withIndex()) {
            condition(item, i)
        }
    }
}

fun <T> List<T>.find(condition: (T) -> Boolean): T? {
    if (isNotEmpty()) {
        for (item in this) {
            if (condition(item)) {
                return item
            }
        }
    }
    return null
}

fun <T> List<T>.findWithIndex(condition: (T) -> Boolean): ListItem<T>? {
    if (isNotEmpty()) {
        for ((i, item) in this.withIndex()) {
            if (condition(item)) {
                return ListItem(item, i)
            }
        }
    }
    return null
}

fun <T> List<T>.findPos(condition: (T) -> Boolean): Int {
    var i = -1
    if (isNotEmpty()) {
        i = 0
        for (item in this) {
            if (condition(item)) {
                return i
            }
            i++
        }
    }
    return i
}

fun <T> List<T>.where(condition: (T) -> Boolean): List<T> {
    val result = ArrayList<T>()
    if (isNotEmpty()) {
        for (item in this) {
            if (condition(item)) {
                result.add(item)
            }
        }
    }
    return result
}
