package com.example.testsapo.utils

import android.view.View

private const val TAG_KEY = 0x7EFFFFFF
fun View.isSingleValid(action: () -> Unit, duration: Long = 200) {
    val curTime = System.currentTimeMillis()
    val tag = this.getTag(TAG_KEY)
    tag?.let {
        val preTime = tag as Long
        if (curTime - preTime <= duration) return
        this.setTag(TAG_KEY, curTime)
        action()
    }
    this.setTag(TAG_KEY, curTime)
    action()
}
