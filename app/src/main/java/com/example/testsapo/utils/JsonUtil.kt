package com.example.testsapo.utils

import com.google.gson.Gson
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.lang.reflect.Type

object JsonUtil {
    fun toString(json: JSONObject?): String {
        return json?.toString() ?: ""
    }

    fun isValid(str: String?): Boolean {
        if (!str.isNullOrEmpty()) {
            try {
                JSONObject(str);
            } catch (e: JSONException) {
                try {
                    JSONArray(str)
                } catch (e: JSONException) {
                    return false
                }
            }
            return true
        }
        return false
    }

    fun <T : String?> T.isJson(): Boolean {
        return JsonUtil.isValid(this)
    }

    inline fun <reified T> from(json: String?, gson: Gson = Gson()): T? =
        if (isValid(json)) gson.fromJson(json, T::class.java) else null

    fun <T> from(json: String?, type: Type, gson: Gson = Gson()): T? =
        if (isValid(json)) gson.fromJson<T>(json, type) else null

    fun <T> to(model: T?, gson: Gson = Gson()): String? = model?.let { gson.toJson(model) }

    fun <T> to(model: T?, type: Type, gson: Gson = Gson()): String? = model?.let { gson.toJson(model, type) }
}
