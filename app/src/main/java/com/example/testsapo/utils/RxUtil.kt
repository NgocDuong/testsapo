package com.example.testsapo.utils

import com.example.testsapo.model.http.exception.ApiException
import com.example.testsapo.model.http.response.BaseHttpResponse
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.FlowableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

object RxUtil {


    fun <T> rxSchedulerHelper(): FlowableTransformer<T, T> {
        return FlowableTransformer { observable ->
            observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        }
    }


    fun <T> handleMyResult(): FlowableTransformer<BaseHttpResponse<T>, T> {
        return FlowableTransformer { baseHttpResponseFlowable ->
            baseHttpResponseFlowable.flatMap { tMyHttpResponse ->
                createData(tMyHttpResponse.Cities)
            }
        }
    }


    fun <T> createData(t: T): Flowable<T> {
        return Flowable.create({ emitter ->
            try {
                emitter.onNext(t)
                emitter.onComplete()
            } catch (e: Exception) {
                emitter.onError(e)
            }
        }, BackpressureStrategy.BUFFER)
    }
}
