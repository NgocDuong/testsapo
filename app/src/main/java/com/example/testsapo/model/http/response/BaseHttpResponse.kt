package com.example.testsapo.model.http.response

class BaseHttpResponse<T> {
    var code: Int = 0
    var message: String? = null
    var Cities: T? = null
    var Districts: T? = null
}
