package com.example.testsapo.model.http

import com.blankj.utilcode.util.NetworkUtils
import com.example.testsapo.BuildConfig
import com.example.testsapo.app.Const
import com.example.testsapo.model.http.api.BaseApi
import com.example.testsapo.model.http.response.BaseHttpResponse
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import io.reactivex.Flowable
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit

object RetrofitHelper {
    private var mBaseApi: BaseApi? = null
    private var okHttpClient: OkHttpClient? = null

    fun getBaseApi(): BaseApi? {
        if (mBaseApi == null) {
            mBaseApi = getRetrofit().create(BaseApi::class.java)
        }
        return mBaseApi
    }

    private fun getRetrofit(): Retrofit {
        initOkHttpClient()
        val gson = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create()
        return Retrofit.Builder()
            .client(okHttpClient!!)
            .baseUrl(BaseApi.HOST)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    private fun initOkHttpClient() {
        if (okHttpClient == null) {
            val builder = OkHttpClient.Builder()
            if (BuildConfig.DEBUG) {
                val loggingInterceptor = HttpLoggingInterceptor()
                loggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
                builder.addInterceptor(loggingInterceptor)
            }
            val cacheFile = File(Const.PATH_CACHE)
            val cache = okhttp3.Cache(cacheFile, (1024 * 1024 * 50).toLong())
            val cacheInterceptor = Interceptor { chain ->
                var request = chain.request()
                if (!NetworkUtils.isConnected()) {
                    request = request.newBuilder()
                        .cacheControl(CacheControl.FORCE_CACHE)
                        .build()
                }
                val response = chain.proceed(request)
                if (NetworkUtils.isConnected()) {
                    val maxAge = 0
                    // When there is a network, it is not cached. The maximum storage time is 0.
                    response.newBuilder()
                        .header("Cache-Control", "public, max-age=$maxAge")
                        .removeHeader("Pragma")
                        .build()
                } else {
                    // Set timeout to 4 weeks when there is no network
                    val maxStale = 60 * 60 * 24 * 28
                    response.newBuilder()
                        .header("Cache-Control", "public, only-if-cached, max-stale=$maxStale")
                        .removeHeader("Pragma")
                        .build()
                }
                response
            }

            builder.addNetworkInterceptor(cacheInterceptor)
            builder.addInterceptor(cacheInterceptor)
            builder.cache(cache)
            //Set timeout
            builder.connectTimeout(10, TimeUnit.SECONDS)
            builder.readTimeout(20, TimeUnit.SECONDS)
            builder.writeTimeout(20, TimeUnit.SECONDS)
            //Error reconnection
            builder.retryOnConnectionFailure(true)
            okHttpClient = builder.build()
        }
    }
}