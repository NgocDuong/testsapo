package com.example.testsapo.model.db.dao

import androidx.room.*
import com.example.testsapo.model.bean.User
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface UserDao {
    @Delete
    fun delete(user: User) : Single<Int>

    @Query("SELECT * FROM users WHERE name LIKE :name LIMIT 1")
    fun findByName(name: String): Flowable<User>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: User): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(users: List<User>): Completable

    @Query("SELECT * FROM users")
    fun loadAll(): Flowable<List<User>>

    @Query("SELECT * FROM users WHERE id IN (:userIds)")
    fun loadAllByIds(userIds: List<Int>): Flowable<List<User>>
}
