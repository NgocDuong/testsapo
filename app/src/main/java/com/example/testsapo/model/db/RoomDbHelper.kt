package com.example.testsapo.model.db

import android.content.Context
import androidx.room.Room
import com.example.testsapo.app.App
import com.example.testsapo.app.Const
import com.example.testsapo.model.bean.User
import io.reactivex.Flowable

class RoomDbHelper {
    companion object {
        @Volatile
        private var instance: AppDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context,
            AppDatabase::class.java, Const.DB_NAME
        ).build()

    }
}
