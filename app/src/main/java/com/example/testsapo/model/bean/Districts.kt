package com.example.testsapo.model.bean

import java.util.*

data class Districts(
    var CityCode: Int,
    var Name: String,
    var DistrictCode: Int
    )