package com.example.testsapo.model.prefs

import android.content.Context
import android.content.SharedPreferences
import com.example.testsapo.app.Const
import com.example.testsapo.utils.JsonUtil
import com.example.testsapo.utils.TypeUtil
import java.util.*

class Pref {

    var preferences: SharedPreferences? = null

    fun init(context: Context) {
        preferences = context.getSharedPreferences(Const.Preference.KEY, Context.MODE_PRIVATE)
    }

    fun put(key: String?, obj: Any?) {
        if (key == null || key.isEmpty() || obj == null) {
            return
        }
        try {
            val editor = preferences?.edit()
            editor?.putString(key, JsonUtil.to(obj))
            editor?.apply()
        } catch (cme: ConcurrentModificationException) {
            cme.printStackTrace()
        }
    }

    fun delete(key: String?) {
        if (key == null || key.isEmpty()) {
            return
        }
        val editor = preferences?.edit()
        editor?.remove(key)
        editor?.apply()
    }

    fun contains(key: String?): Boolean {
        return preferences?.contains(key) == true
    }

    inline fun <reified T> get(key: String): T? {
        val str = preferences?.getString(key, null)
        return if (str == null) null else {
            try {
                JsonUtil.from<T>(str)
            } catch (e: Exception) {
                null
            }

        }
    }

    inline fun <reified E, reified T> getGeneric(key: String?): E? {
        val str = preferences?.getString(key, null)

        return str?.let {
            try {
                JsonUtil.from<E>(it, TypeUtil.getType(E::class.java, T::class.java))
            } catch (e: Exception) {
                null
            }
        }
    }

    inline fun <reified T> getList(key: String?): List<T>? {
        return getGeneric<List<T>, T>(key)
    }

    inline fun <reified E, reified T> getGenericList(key: String): E? {
        val str = preferences?.getString(key, null)

        return str?.let {
            try {
                JsonUtil.from<E>(it, TypeUtil.getType(E::class.java, TypeUtil.getType(List::class.java, T::class.java)))
            } catch (e: Exception) {
                null
            }
        }
    }
}
