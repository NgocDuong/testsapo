package com.example.testsapo.model.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.testsapo.model.bean.User
import com.example.testsapo.model.db.dao.UserDao

@Database(entities = [User::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
}
