package com.example.testsapo.model.bean

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users")
class User(
    @field:ColumnInfo(name = "created_at")
    var createdAt: String,
    @field:PrimaryKey
    var id: Long?, var name: String,
    @field:ColumnInfo(name = "updated_at")
    var updatedAt: String
)
