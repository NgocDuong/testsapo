package com.example.testsapo.model.http.exception

class ApiException : Exception {

    var code: Int = 0

    constructor(msg: String) : super(msg) {}

    constructor(msg: String, code: Int) : super(msg) {
        this.code = code
    }
}
