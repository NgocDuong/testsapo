package com.example.testsapo.model.bean

import java.util.*

data class Cities(
    val Name: String,
    val CityCode: Int
    )