package com.example.testsapo.model.http.api

import com.example.testsapo.model.bean.Cities
import com.example.testsapo.model.bean.Districts
import com.example.testsapo.model.http.response.BaseHttpResponse
import com.google.gson.JsonElement
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.POST

interface BaseApi {
    companion object {
        val HOST = "https://raw.githubusercontent.com/"
    }


    @GET("sapo-tech/home_test_mobile/master/Cities.json")
    fun getCities(): Flowable<BaseHttpResponse<List<Cities>>>

    @GET("sapo-tech/home_test_mobile/master/Districts.json")
    fun getDistricts(): Flowable<BaseHttpResponse<List<Districts>>>
}
