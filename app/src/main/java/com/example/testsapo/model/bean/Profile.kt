package com.example.testsapo.model.bean

data class Profile(
    var Name: String?,
    var Email: String?,
    var Password: String?,
    var Gender: String?,
    var YearOld: String?,
    var Citie: String?,
    var District: String?
    )