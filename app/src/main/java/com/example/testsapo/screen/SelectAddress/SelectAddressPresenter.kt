package com.example.testsapo.screen.SelectAddress

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.example.testsapo.base.RxPresenter
import com.example.testsapo.model.bean.Cities
import com.example.testsapo.model.bean.Districts
import com.example.testsapo.model.http.RetrofitHelper
import com.example.testsapo.utils.RxUtil

class SelectAddressPresenter : RxPresenter<SelectAddressContact.View>(),
    SelectAddressContact.Presenter {
    var listCitie:List<Cities> = emptyList()
    var listDistricts: ArrayList<Districts>? = null
    override fun getCities() {
        mView?.showProgressDialog()
        RetrofitHelper.getBaseApi()?.getCities()
            ?.compose(RxUtil.rxSchedulerHelper())
            ?.subscribe({
                mView?.dismissProgressDialog()
                Log.d("DUONGNN", it.toString())
                listCitie = it?.Cities!!
                mView?.setContentsCities(listCitie)
            }, {
                mView?.dismissProgressDialog()
                it.printStackTrace()
            })?.let {
                addSubscribe(it)
            }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun getDistricts(CityCode:Int) {
        mView?.showProgressDialog()
        RetrofitHelper.getBaseApi()?.getDistricts()
            ?.compose(RxUtil.rxSchedulerHelper())

            ?.subscribe({
                mView?.dismissProgressDialog()
                listDistricts = it.Districts as ArrayList<Districts>?
                listDistricts?.removeIf {
                    it.CityCode != CityCode
                }
                mView?.setContentsCities(emptyList())
                listDistricts?.let { it1 -> mView?.setContentsDistricts(it1) }

            }, {
                mView?.dismissProgressDialog()
                it.printStackTrace()
            })?.let {
                addSubscribe(it)
            }
    }

}