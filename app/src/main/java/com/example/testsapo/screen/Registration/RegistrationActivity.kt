package com.example.testsapo.screen.Registration

import com.example.testsapo.base.BaseActivity
import com.example.testsapo.R
import kotlinx.android.synthetic.main.activity_registration.*
import java.util.regex.Pattern
import android.content.Intent
import com.example.testsapo.app.Const
import com.example.testsapo.screen.SelectAddress.SelectAddressActivity


class RegistrationActivity : BaseActivity<RegistrationPresenter>(), RegistrationContact.View {
    override val layout: Int = R.layout.activity_registration

    override fun getPresenter(): RegistrationPresenter = RegistrationPresenter()


    override fun initEventAndData() {
        clickListener()
    }

    private fun clickListener() {
        btnNext.setOnClickListener {
            if(checkValidator()){
                Const.profile?.Name = edtUsename.text.toString()
                Const.profile?.Email = edtEmail.text.toString()
                Const.profile?.Password = edtPassword.text.toString()
                val i = Intent(this, SelectAddressActivity::class.java)
                startActivity(i)
            }
        }

    }




    fun checkValidator(): Boolean {
        var isValid = true
        if (edtUsename.text.length<8){
            //edtUsename.setTextColor(resources.getColor(R.color.redcolor))
            txtUsername.text = getString(R.string.notiUsername)
            isValid = false
        }else{
            edtUsename.setTextColor(resources.getColor(R.color.greenColor))
            txtUsername.text = ""
        }

        if (edtEmail.text.isEmpty()){
            txtEmail.text = "Email không được để trống"
            isValid = false
        }
        else if(isEmailValid(edtEmail.text.toString())){
            edtEmail.setTextColor(resources.getColor(R.color.greenColor))
            txtEmail.text = ""
        }else{
            //edtEmail.setTextColor(resources.getColor(R.color.redcolor))
            txtEmail.text = "Email không hợp lệ"
            isValid = false
        }


        if (edtPassword.text.isEmpty()){
            txtPassword.text = "Password không được để trống"
            isValid = false
        }
        else if(passwordValidate(edtPassword.text.toString())){
            edtPassword.setTextColor(resources.getColor(R.color.greenColor))
            txtPassword.text = ""
        }else{
            //edtPassword.setTextColor(resources.getColor(R.color.redcolor))
            txtPassword.text = "Password không hợp lệ"
            isValid = false
        }


        if (edtRepeatPassword.text.isEmpty()){
            txtRepeatPassword.text = "Repeat Password không được để trống"
            isValid = false
        }
        else if(edtRepeatPassword.text.toString() == edtPassword.text.toString()){
            edtRepeatPassword.setTextColor(resources.getColor(R.color.greenColor))
            txtRepeatPassword.text = ""
        }else{
            //edtRepeatPassword.setTextColor(resources.getColor(R.color.redcolor))
            txtRepeatPassword.text = "repeat password phải trùng với password"
            isValid = false
        }

        return isValid
    }

    fun isEmailValid(email: String): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9]))|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }

    fun passwordValidate(password: String): Boolean {

        if (password.length<8){
            return false
        }

        val letter = Pattern.compile("[a-z]")
        val Letter = Pattern.compile("[A-Z]")
        val digit = Pattern.compile("[0-9]")
        val special = Pattern.compile("[!@#$%&*()_+=|<>?{}\\[\\]~-]")

        val hasLetter = letter.matcher(password)
        val HasLetter = Letter.matcher(password)
        val hasDigit = digit.matcher(password)
        val hasSpecial = special.matcher(password)

        return hasLetter.find() && hasDigit.find() && hasSpecial.find() && HasLetter.find()
    }

}