package com.example.testsapo.screen.SelectGeneral

import com.example.testsapo.base.BasePresenter
import com.example.testsapo.base.BaseView

public interface SelectGeneralContact {
    interface View : BaseView {
    }

    interface Presenter : BasePresenter<View> {

    }
}