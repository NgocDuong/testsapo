package com.example.testsapo.screen.Overall

import com.example.testsapo.base.BasePresenter
import com.example.testsapo.base.BaseView
import com.example.testsapo.model.bean.User


public interface OverallContact {
    interface View : BaseView {
    }

    interface Presenter : BasePresenter<View> {

    }
}