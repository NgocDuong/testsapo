package com.example.testsapo.screen.SelectAddress

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.example.testsapo.R
import com.example.testsapo.model.bean.Cities


class SelectAddressAdapter(layoutRes: Int, data: List<Cities>?) :
    BaseQuickAdapter<Cities, BaseViewHolder>(layoutRes, data) {
    override fun convert(helper: BaseViewHolder, item: Cities?) {
        helper.setText(R.id.txtAddress, item?.Name)

    }

}