package com.example.testsapo.screen.SelectAddress

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.example.testsapo.R
import com.example.testsapo.model.bean.Districts


class SelectDistrictsAdapter(layoutRes: Int, data: List<Districts>?) :
    BaseQuickAdapter<Districts, BaseViewHolder>(layoutRes, data) {
    override fun convert(helper: BaseViewHolder, item: Districts?) {
        helper.setText(R.id.txtAddress, item?.Name)

    }

}