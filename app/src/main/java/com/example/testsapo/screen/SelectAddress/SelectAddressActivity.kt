package com.example.testsapo.screen.SelectAddress

import android.content.Intent
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testsapo.R
import com.example.testsapo.base.BaseActivity
import com.example.testsapo.model.bean.Cities
import com.example.testsapo.model.bean.Districts
import kotlinx.android.synthetic.main.activity_selectaddress.*
import android.view.KeyEvent
import com.example.testsapo.app.Const
import com.example.testsapo.screen.SelectGeneral.SelectGeneralActivity


class SelectAddressActivity : BaseActivity<SelectAddressPresenter>(), SelectAddressContact.View {


    lateinit var mAdapter: SelectAddressAdapter
    lateinit var mAdapterDistricts: SelectDistrictsAdapter
    var listCities : List<Cities> = emptyList()
    var listDistricts : List<Districts> = emptyList()
    var isSelectCities = true
    override val layout: Int = R.layout.activity_selectaddress

    override fun getPresenter(): SelectAddressPresenter = SelectAddressPresenter()


    @RequiresApi(Build.VERSION_CODES.N)
    override fun initEventAndData() {
        showlistCities()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun showlistCities(){
        isSelectCities = true

        textView.text = "Bạn hãy chọn\nTỉnh/ Thành Phố nơi bạn ở"
        mAdapter = SelectAddressAdapter(R.layout.item_select_address, null)
        recycler_view.apply {
            adapter = mAdapter
            layoutManager = LinearLayoutManager(mContext)

        }


        mAdapterDistricts = SelectDistrictsAdapter(R.layout.item_select_address, null)


        mPresenter?.getCities()

        mAdapter.setOnItemClickListener { a, view, position ->
            Const.profile?.Citie = listCities[position].Name
            showlistDistricts(position)

        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun showlistDistricts(i:Int){
        textView.text = "Bạn hãy chọn\nQuận/Huyện nơi bạn ở"
        isSelectCities = false
        recycler_view.apply {
            adapter = mAdapterDistricts
            layoutManager = LinearLayoutManager(mContext)

        }
        mPresenter?.getDistricts(listCities[i].CityCode)
        mAdapterDistricts.setOnItemClickListener { adapter, view, position ->
            Const.profile?.District = listDistricts[position].Name
            val i = Intent(this, SelectGeneralActivity::class.java)
            startActivity(i)
        }
    }

    override fun setContentsCities(contents: List<Cities>) {
        listCities = contents
        mAdapter.setNewData(contents)

    }
    override fun setContentsDistricts(contents: List<Districts>) {
        listDistricts = contents
        mAdapterDistricts.setNewData(contents)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() === 0) {
            if(isSelectCities){
                finish()
            }else{
                showlistCities()
            }
            true
        } else super.onKeyDown(keyCode, event)

    }

}