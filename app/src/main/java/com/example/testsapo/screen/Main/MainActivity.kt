package com.example.testsapo.screen.Main

import com.example.testsapo.R
import com.example.testsapo.base.BaseActivity

class MainActivity : BaseActivity<MainPresenter>(), MainContact.View {
    override val layout: Int = R.layout.activity_main

    override fun getPresenter(): MainPresenter = MainPresenter()


    override fun initEventAndData() {

    }

}