package com.example.testsapo.screen.Overall

import android.content.Intent
import com.example.testsapo.R
import com.example.testsapo.app.Const
import com.example.testsapo.base.BaseActivity
import com.example.testsapo.screen.Welcome.WelcomeActivity

import kotlinx.android.synthetic.main.activity_overall.*
import kotlinx.android.synthetic.main.activity_overall.txtEmail
import kotlinx.android.synthetic.main.activity_overall.txtUsername


class OverallActivity : BaseActivity<OverallPresenter>(), OverallContact.View {
    override val layout: Int = R.layout.activity_overall
    val emptyString = ""
    override fun getPresenter(): OverallPresenter = OverallPresenter()


    override fun initEventAndData() {
        showData()
        btnNext.setOnClickListener {
            val i = Intent(this, WelcomeActivity::class.java)
            startActivity(i)
        }
    }

    private fun showData() {
        txtUsername.text = Const.profile?.Name ?: emptyString
        txtEmail.text = Const.profile?.Email ?: emptyString
        txtGender.text = Const.profile?.Gender ?: emptyString
        txtYearOld.text = Const.profile?.YearOld ?: emptyString
        txtCitie.text = Const.profile?.Citie ?: emptyString
        txtDistrict.text = Const.profile?.District ?: emptyString
    }

}