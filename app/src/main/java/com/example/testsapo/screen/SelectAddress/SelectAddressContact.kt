package com.example.testsapo.screen.SelectAddress

import com.example.testsapo.base.BasePresenter
import com.example.testsapo.base.BaseView
import com.example.testsapo.model.bean.Cities
import com.example.testsapo.model.bean.Districts

public interface SelectAddressContact {
    interface View : BaseView {
        fun setContentsCities(contents: List<Cities>)
        fun setContentsDistricts(contents: List<Districts>)
    }

    interface Presenter : BasePresenter<View> {
        fun getCities()
        fun getDistricts(CityCode:Int)
    }
}