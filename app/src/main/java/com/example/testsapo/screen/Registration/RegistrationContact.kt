package com.example.testsapo.screen.Registration

import com.example.testsapo.base.BasePresenter
import com.example.testsapo.base.BaseView
import com.example.testsapo.model.bean.User


public interface RegistrationContact {
    interface View : BaseView {
    }

    interface Presenter : BasePresenter<View> {

    }
}