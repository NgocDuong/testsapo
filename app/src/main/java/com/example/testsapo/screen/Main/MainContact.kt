package com.example.testsapo.screen.Main

import com.example.testsapo.base.BasePresenter
import com.example.testsapo.base.BaseView
import com.example.testsapo.model.bean.User


public interface MainContact {
    interface View : BaseView {
    }

    interface Presenter : BasePresenter<View> {

    }
}