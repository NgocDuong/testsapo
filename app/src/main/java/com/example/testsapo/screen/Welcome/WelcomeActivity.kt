package com.example.testsapo.screen.Welcome

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.testsapo.R

class WelcomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
    }
}
