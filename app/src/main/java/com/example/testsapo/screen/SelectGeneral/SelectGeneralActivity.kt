package com.example.testsapo.screen.SelectGeneral

import android.content.Intent
import android.util.Log
import com.blankj.utilcode.util.ToastUtils
import com.example.testsapo.R
import com.example.testsapo.app.Const
import com.example.testsapo.base.BaseActivity
import com.example.testsapo.screen.Overall.OverallActivity
import kotlinx.android.synthetic.main.activity_selectgeneral.*

class SelectGeneralActivity : BaseActivity<SelectGeneralPresenter>(), SelectGeneralContact.View {
    var numberPickerDefault = 10
    override val layout: Int = R.layout.activity_selectgeneral

    override fun getPresenter(): SelectGeneralPresenter = SelectGeneralPresenter()


    override fun initEventAndData() {
        numberPicker.maxValue = 100
        numberPicker.minValue = 0
        numberPicker.value = numberPickerDefault
        Const.profile?.YearOld = numberPickerDefault.toString()
        numberPicker.setOnValueChangedListener { picker, oldVal, newVal ->
            Const.profile?.YearOld = newVal.toString()
        }

        radioGroup.setOnCheckedChangeListener { radioGroup, i ->
            when (i) {
                R.id.radioButtonMen -> {
                    Const.profile?.Gender = "Nam"
                }
                R.id.radioButtonWomen -> {
                    Const.profile?.Gender = "Nữ"
                }
                else -> {
                    Const.profile?.Gender = "Khác"
                }
            }
        }

        btnNext.setOnClickListener {
            val i = Intent(this, OverallActivity::class.java)
            startActivity(i)
        }
    }

}